package co.rajat.covid19.dashboard.services;

import co.rajat.covid19.dashboard.models.ChartType;
import co.rajat.covid19.dashboard.models.Coordinate;
import co.rajat.covid19.dashboard.models.Covid19Aggregate;
import co.rajat.covid19.dashboard.repositories.Covid19AggregateRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {ChartService.class})
public class ChartServiceTest {

    @Autowired
    private ChartService chartService;

    @MockBean
    private Covid19AggregateRepository covid19AggregateRepository;

    private final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Before
    public void setUp() {
        Covid19Aggregate aggregateIn = new Covid19Aggregate();
        aggregateIn.setDate(LocalDate.now());
        aggregateIn.setTotalConfirmed(1000);
        aggregateIn.setTotalDeceased(300);
        aggregateIn.setTotalRecovered(200);
        aggregateIn.setConfirmed(100);
        aggregateIn.setDeceased(100);
        aggregateIn.setRecovered(100);

        Mockito.when(covid19AggregateRepository.findByDateAfterOrderByDate(LocalDate.now().minusDays(60)))
                .thenReturn(Collections.singletonList(aggregateIn));
    }

    @Test
    public void testActiveService() {
        // when
        List<Coordinate> found = chartService.getChartData(ChartType.ACTIVE);

        // then
        assertThat(found.size())
                .isEqualTo(1);
        assertThat(found.get(0).getX())
                .isEqualTo(LocalDate.now().format(dateTimeFormat));
        assertThat(found.get(0).getY())
                .isEqualTo("800");
    }

    @Test
    public void testConfirmedService() {
        // when
        List<Coordinate> found = chartService.getChartData(ChartType.TOTAL);

        // then
        assertThat(found.size())
                .isEqualTo(1);
        assertThat(found.get(0).getX())
                .isEqualTo(LocalDate.now().format(dateTimeFormat));
        assertThat(found.get(0).getY())
                .isEqualTo("1000");
    }

    @Test
    public void testDeceasedService() {
        // when
        List<Coordinate> found = chartService.getChartData(ChartType.DEATHS);

        // then
        assertThat(found.size())
                .isEqualTo(1);
        assertThat(found.get(0).getX())
                .isEqualTo(LocalDate.now().format(dateTimeFormat));
        assertThat(found.get(0).getY())
                .isEqualTo("300");
    }

    @Test
    public void testRecoveredService() {
        // when
        List<Coordinate> found = chartService.getChartData(ChartType.RECOVERED);

        // then
        assertThat(found.size())
                .isEqualTo(1);
        assertThat(found.get(0).getX())
                .isEqualTo(LocalDate.now().format(dateTimeFormat));
        assertThat(found.get(0).getY())
                .isEqualTo("200");
    }
}
