package co.rajat.covid19.dashboard.deserializers;

import co.rajat.covid19.dashboard.models.Covid19StateAggregate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class LocalDateTimeDeserializerTest {

    private final String ex1 = "{\"active\": \"4488\",\"confirmed\": \"8187\",\"deaths\": \"396\",\"deltaconfirmed\": \"0\",\"deltadeaths\": \"0\",\"deltarecovered\": \"0\",\"lastupdatedtime\": \"07/06/2020 19:11:47\",\"recovered\": \"3303\",\"state\": \"West Bengal\",\"statecode\": \"WB\"}";

    @Test
    public void deserialize() throws JsonProcessingException {

        // when
        Covid19StateAggregate aggregate = new ObjectMapper().readValue(ex1, Covid19StateAggregate.class);

        // then  07/06/2020 19:11:47
        assertThat(aggregate.getLastUpdated())
                .isEqualTo(LocalDateTime.of(2020, 6, 7, 19, 11, 47));

    }
}