package co.rajat.covid19.dashboard.deserializers;

import co.rajat.covid19.dashboard.models.Covid19TestsAggregate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class IntDeserializerTest {

    private final String ex1 = "{\"samplereportedtoday\": \"12,354\",\"testspermillion\": \"5\",\"totalsamplestested\": \"6500\",\"updatetimestamp\": \"13/03/2020 00:00:00\"}";
    private final String ex2 = "{\"samplereportedtoday\": \"\",\"testspermillion\": \"5\",\"totalsamplestested\": \"6500\",\"updatetimestamp\": \"13/03/2020 00:00:00\"}";

    @Test
    public void deserialize() throws JsonProcessingException {

        // when
        Covid19TestsAggregate aggregate = new ObjectMapper().readValue(ex1, Covid19TestsAggregate.class);

        // then
        assertThat(aggregate.getSamplesTestedToday())
                .isEqualTo(12354);
    }

    @Test
    public void deserialize_emptyString() throws JsonProcessingException {

        // when
        Covid19TestsAggregate aggregate = new ObjectMapper().readValue(ex2, Covid19TestsAggregate.class);

        // then
        assertThat(aggregate.getSamplesTestedToday())
                .isEqualTo(0);
    }

}