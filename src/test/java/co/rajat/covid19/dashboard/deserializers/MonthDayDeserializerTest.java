package co.rajat.covid19.dashboard.deserializers;

import co.rajat.covid19.dashboard.models.Covid19Aggregate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class MonthDayDeserializerTest {

    private final String ex1 = "{\"dailyconfirmed\": \"1\",\"dailydeceased\": \"0\",\"dailyrecovered\": \"0\",\"date\": \"30 January \",\"totalconfirmed\": \"1\",\"totaldeceased\": \"0\",\"totalrecovered\": \"0\"}";

    @Test
    public void deserialize() throws JsonProcessingException {

        // when
        Covid19Aggregate aggregate = new ObjectMapper().readValue(ex1, Covid19Aggregate.class);

        // then
        assertThat(aggregate.getDate())
                .isEqualTo(LocalDate.of(2020,1,30));
    }

}
