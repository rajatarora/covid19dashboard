package co.rajat.covid19.dashboard.repositories;

import co.rajat.covid19.dashboard.models.Covid19Aggregate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class Covid19AggregateRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private Covid19AggregateRepository repository;

    @Test
    public void whenFindByDateAfter_thenReturnAggregate() {
        // given
        Covid19Aggregate aggregateIn = new Covid19Aggregate();
        aggregateIn.setDate(LocalDate.now().plusDays(1));
        aggregateIn.setTotalConfirmed(1000);
        aggregateIn.setTotalDeceased(1000);
        aggregateIn.setTotalRecovered(1000);
        aggregateIn.setConfirmed(100);
        aggregateIn.setDeceased(100);
        aggregateIn.setRecovered(100);
        entityManager.persist(aggregateIn);
        entityManager.flush();

        // when
        Covid19Aggregate aggregateOut = repository.findByDateAfterOrderByDate(LocalDate.now()).get(0);

        // then
        assertThat(aggregateOut.getDate())
                .isEqualTo(aggregateIn.getDate());
        assertThat(aggregateOut.getTotalConfirmed())
                .isEqualTo(aggregateIn.getTotalConfirmed());
        assertThat(aggregateOut.getTotalDeceased())
                .isEqualTo(aggregateIn.getTotalDeceased());
        assertThat(aggregateOut.getTotalRecovered())
                .isEqualTo(aggregateIn.getTotalRecovered());
        assertThat(aggregateOut.getConfirmed())
                .isEqualTo(aggregateIn.getConfirmed());
        assertThat(aggregateOut.getRecovered())
                .isEqualTo(aggregateIn.getRecovered());
        assertThat(aggregateOut.getDeceased())
                .isEqualTo(aggregateIn.getDeceased());
    }
}