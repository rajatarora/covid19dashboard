package co.rajat.covid19.dashboard.repositories;

import co.rajat.covid19.dashboard.models.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    UserRepository repository;

    @Test
    public void whenFindByEmail_thenReturnUser() {
        // given
        User userIn = new User();
        userIn.setEmail("r@rajat.co");
        userIn.setName("Rajat Arora");
        userIn.setPassword("rajat123");
        userIn.setPasswordMatch("rajat123");
        entityManager.persist(userIn);
        entityManager.flush();

        // when
        User userOut = repository.findByEmail("r@rajat.co");

        // then
        assertThat(userOut.getEmail())
                .isEqualTo(userIn.getEmail());
        assertThat(userOut.getName())
                .isEqualTo(userIn.getName());
        assertThat(userOut.getPassword())
                .isEqualTo(userIn.getPassword());
        assertThat(userOut.getPasswordMatch())
                .isEqualTo(userIn.getPasswordMatch());
    }
}