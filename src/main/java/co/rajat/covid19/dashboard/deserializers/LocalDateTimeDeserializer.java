package co.rajat.covid19.dashboard.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Creates LocalDateTime out of strings like "06/01/1990 20:04:00".
 */
public class LocalDateTimeDeserializer extends StdDeserializer<LocalDateTime> {

    private static final long serialVersionUID = 1L;
    private final String formatterPattern = "dd/MM/yyyy HH:mm:ss";

    protected LocalDateTimeDeserializer() {
        super(LocalDate.class);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(this.formatterPattern);
        return LocalDateTime.parse(jp.readValueAs(String.class), formatter);
    }

}
