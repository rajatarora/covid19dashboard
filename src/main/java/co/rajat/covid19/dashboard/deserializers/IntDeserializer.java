package co.rajat.covid19.dashboard.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

/**
 * Deserializes integers which have commas inserted in them as separators
 */
public class IntDeserializer extends StdDeserializer<Integer> {

    private static final long serialVersionUID = 1L;

    protected IntDeserializer() {
        super(Integer.class);
    }

    @Override
    public Integer deserialize(JsonParser jsonParser,
                               DeserializationContext deserializationContext)
            throws IOException {
        String intString = jsonParser.getText();
        if (intString.contains(",")) {
            intString = intString.replace(",","");
        }
        if (intString.isEmpty()) {
            return 0;
        }
        return Integer.valueOf(intString);
    }
}
