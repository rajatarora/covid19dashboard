package co.rajat.covid19.dashboard.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.MonthDay;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

/**
 * Creates LocalDate out of strings like "6 January". Year is taken to be current.
 */
public class MonthDayDeserializer extends StdDeserializer<LocalDate> {

    private static final long serialVersionUID = 1L;
    private final String formatterPattern = "d LLLL";

    protected MonthDayDeserializer() {
        super(LocalDate.class);
    }

    @Override
    public LocalDate deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(this.formatterPattern);
        TemporalAccessor temporalAccessor = formatter.parse(jp.readValueAs(String.class).trim());
        MonthDay monthDay = MonthDay.from(temporalAccessor);
        return monthDay.atYear(LocalDate.now().getYear());
    }

}
