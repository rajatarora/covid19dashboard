package co.rajat.covid19.dashboard.services;

import co.rajat.covid19.dashboard.exceptions.UserAlreadyExistsException;
import co.rajat.covid19.dashboard.models.User;
import co.rajat.covid19.dashboard.models.UserDetailsImpl;
import co.rajat.covid19.dashboard.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(s);

        if (user == null) {
            throw new UsernameNotFoundException("User not found.");
        }

        return new UserDetailsImpl(user);
    }

    public void registerNewAccount(User user) throws UserAlreadyExistsException {

        User existing = userRepository.findByEmail(user.getEmail());
        if (existing != null) {
            throw new UserAlreadyExistsException("User already exists");
        }

        userRepository.save(user);

    }
}
