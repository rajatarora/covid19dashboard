package co.rajat.covid19.dashboard.services;

import co.rajat.covid19.dashboard.models.ChartType;
import co.rajat.covid19.dashboard.models.Coordinate;
import co.rajat.covid19.dashboard.models.Covid19Aggregate;
import co.rajat.covid19.dashboard.repositories.Covid19AggregateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChartService {

    @Autowired
    private Covid19AggregateRepository covid19AggregateRepository;
    private final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public List<Coordinate> getChartData(ChartType chartType) {
        LocalDate from = LocalDate.now().minusDays(60);
        List<Covid19Aggregate> result = covid19AggregateRepository.findByDateAfterOrderByDate(from);
        List<Coordinate> coordinates = new ArrayList<>();

        switch (chartType) {

            case ACTIVE:
                result.forEach(row -> coordinates.add(new Coordinate(row.getDate().format(dateTimeFormat), Integer.toString(row.getTotalConfirmed() - row.getTotalRecovered()))));
                break;
            case DEATHS:
                result.forEach(row -> coordinates.add(new Coordinate(row.getDate().format(dateTimeFormat), Integer.toString(row.getTotalDeceased()))));
                break;
            case RECOVERED:
                result.forEach(row -> coordinates.add(new Coordinate(row.getDate().format(dateTimeFormat), Integer.toString(row.getTotalRecovered()))));
                break;
            case TOTAL:
                result.forEach(row -> coordinates.add(new Coordinate(row.getDate().format(dateTimeFormat), Integer.toString(row.getTotalConfirmed()))));
                break;
            case STATE:
                break;

        }


        return coordinates;
    }

}
