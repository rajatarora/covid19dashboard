package co.rajat.covid19.dashboard.services;

import co.rajat.covid19.dashboard.Covid19ApiConsumer;
import co.rajat.covid19.dashboard.models.Covid19Aggregate;
import co.rajat.covid19.dashboard.models.Covid19ApiResponse;
import co.rajat.covid19.dashboard.repositories.Covid19AggregateRepository;
import co.rajat.covid19.dashboard.repositories.Covid19StateAggregateRepository;
import co.rajat.covid19.dashboard.repositories.Covid19TestsAggregateRepository;
import org.springframework.stereotype.Service;

@Service
public class Covid19DataService {

    private final Covid19ApiConsumer covid19ApiConsumer;
    private final Covid19AggregateRepository covid19AggregateRepository;
    private final Covid19StateAggregateRepository covid19StateAggregateRepository;
    private final Covid19TestsAggregateRepository covid19TestsAggregateRepository;

    public Covid19DataService(Covid19ApiConsumer covid19ApiConsumer,
                              Covid19AggregateRepository covid19AggregateRepository,
                              Covid19StateAggregateRepository covid19StateAggregateRepository,
                              Covid19TestsAggregateRepository covid19TestsAggregateRepository) {
        this.covid19ApiConsumer = covid19ApiConsumer;
        this.covid19AggregateRepository = covid19AggregateRepository;
        this.covid19StateAggregateRepository = covid19StateAggregateRepository;
        this.covid19TestsAggregateRepository = covid19TestsAggregateRepository;
    }

    public Covid19ApiResponse getData() {
        return covid19ApiConsumer.getCovid19Data();
    }

    public void saveData(Covid19ApiResponse covid19ApiResponse) {
        for (Covid19Aggregate covid19Aggregate : covid19ApiResponse.getCasesTimeSeries()) {
            covid19AggregateRepository.save(covid19Aggregate);
        }
    }
}
