package co.rajat.covid19.dashboard.models;

import co.rajat.covid19.dashboard.deserializers.LocalDateTimeDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity
public class Covid19StateAggregate {

    @JsonProperty("statecode") //TODO this needs to be an enum
    private String statecode;

    @JsonProperty("state")
    private String state;

    @JsonProperty("active")
    private int active;

    @JsonProperty("confirmed")
    private int confirmed;

    @JsonProperty("deaths")
    private int deceased;

    @JsonProperty("recovered")
    private int recovered;

    @JsonProperty("deltaconfirmed")
    private int deltaConfirmed;

    @JsonProperty("deltadeaths")
    private int deltaDeceased;

    @JsonProperty("deltarecovered")
    private int deltaRecovered;

    @JsonProperty("lastupdatedtime")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Id
    private LocalDateTime lastUpdated;

}