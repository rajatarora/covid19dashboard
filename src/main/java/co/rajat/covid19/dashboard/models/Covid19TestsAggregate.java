package co.rajat.covid19.dashboard.models;

import co.rajat.covid19.dashboard.deserializers.IntDeserializer;
import co.rajat.covid19.dashboard.deserializers.LocalDateTimeDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Entity
public class Covid19TestsAggregate {

    @JsonProperty("samplereportedtoday")
    @JsonDeserialize(using = IntDeserializer.class)
    private int samplesTestedToday;

    @JsonProperty("testspermillion")
    private float testsPerMillion;

    @JsonProperty("totalsamplestested")
    public int totalSamplesTested;

    @JsonProperty("updatetimestamp")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Id
    public LocalDateTime updateTimeStamp;

}
