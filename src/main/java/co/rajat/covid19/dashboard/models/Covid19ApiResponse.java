package co.rajat.covid19.dashboard.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Covid19ApiResponse {

    @JsonProperty("cases_time_series")
    private List<Covid19Aggregate> casesTimeSeries;

    @JsonProperty("statewise")
    private List<Covid19StateAggregate> statewise;

    @JsonProperty("tested")
    private List<Covid19TestsAggregate> tested;

}
