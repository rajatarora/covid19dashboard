package co.rajat.covid19.dashboard.models;

public enum ChartType {

    ACTIVE,
    DEATHS,
    RECOVERED,
    TOTAL,
    STATE

}
