package co.rajat.covid19.dashboard.models;

import lombok.Data;

@Data
public class Coordinate {

    private String x;
    private String y;

    public Coordinate(String x, String y) {
        this.x = x;
        this.y = y;
    }
}
