package co.rajat.covid19.dashboard.models;

import co.rajat.covid19.dashboard.validators.PasswordMatches;
import co.rajat.covid19.dashboard.validators.ValidEmail;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "user_details")
@PasswordMatches
public class User {

    @NotNull
    @NotEmpty
    @Id
    @ValidEmail
    private String email;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    @NotEmpty
    private String password;

    private String passwordMatch;

}
