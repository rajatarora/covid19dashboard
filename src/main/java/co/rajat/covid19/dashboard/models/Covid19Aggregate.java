package co.rajat.covid19.dashboard.models;

import co.rajat.covid19.dashboard.deserializers.MonthDayDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Data
@Entity
public class Covid19Aggregate {

    @JsonProperty("date")
    @JsonDeserialize(using = MonthDayDeserializer.class)
    @Id
    private LocalDate date;

    @JsonProperty("dailyconfirmed")
    private int confirmed;

    @JsonProperty("dailydeceased")
    private int deceased;

    @JsonProperty("dailyrecovered")
    private int recovered;

    @JsonProperty("totalconfirmed")
    private int totalConfirmed;

    @JsonProperty("totaldeceased")
    private int totalDeceased;

    @JsonProperty("totalrecovered")
    private int totalRecovered;

}
