package co.rajat.covid19.dashboard.repositories;

import co.rajat.covid19.dashboard.models.Covid19TestsAggregate;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;

public interface Covid19TestsAggregateRepository extends CrudRepository<Covid19TestsAggregate, LocalDateTime> {

}
