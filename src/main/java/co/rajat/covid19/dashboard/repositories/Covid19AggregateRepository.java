package co.rajat.covid19.dashboard.repositories;

import co.rajat.covid19.dashboard.models.Covid19Aggregate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface Covid19AggregateRepository extends CrudRepository<Covid19Aggregate, LocalDate> {

    List<Covid19Aggregate> findByDateAfterOrderByDate(LocalDate date);

}
