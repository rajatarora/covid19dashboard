package co.rajat.covid19.dashboard.repositories;

import co.rajat.covid19.dashboard.models.Covid19StateAggregate;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDateTime;

public interface Covid19StateAggregateRepository extends CrudRepository<Covid19StateAggregate, LocalDateTime> {

}
