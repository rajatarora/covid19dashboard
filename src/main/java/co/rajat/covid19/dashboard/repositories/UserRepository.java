package co.rajat.covid19.dashboard.repositories;

import co.rajat.covid19.dashboard.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {

    User findByEmail(String email);
    
}
