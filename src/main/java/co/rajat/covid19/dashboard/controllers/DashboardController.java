package co.rajat.covid19.dashboard.controllers;

import co.rajat.covid19.dashboard.exceptions.UserAlreadyExistsException;
import co.rajat.covid19.dashboard.models.User;
import co.rajat.covid19.dashboard.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class DashboardController {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @GetMapping("/register")
    public String showRegistrationForm(WebRequest request, Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "register";
    }

    @PostMapping("/register")
    public ModelAndView registerUserAccount(
            @ModelAttribute("user") @Valid User user, BindingResult result,
            HttpServletRequest request, Errors errors, ModelMap model) {

        try {
            userDetailsService.registerNewAccount(user);
        } catch (UserAlreadyExistsException e) {
            ModelAndView mav = new ModelAndView("register", "user", user);
            result.rejectValue("email", "email.notvalid", "This email is already registered");
            return mav;
        }
        model.addAttribute("register", true);
        return new ModelAndView("login", model);
    }

}
