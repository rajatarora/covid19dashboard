package co.rajat.covid19.dashboard.controllers;

import co.rajat.covid19.dashboard.models.ChartType;
import co.rajat.covid19.dashboard.models.Coordinate;
import co.rajat.covid19.dashboard.models.Covid19ApiResponse;
import co.rajat.covid19.dashboard.services.ChartService;
import co.rajat.covid19.dashboard.services.Covid19DataService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DataController {

    private final Covid19DataService covid19DataService;
    private final ChartService chartService;

    public DataController(Covid19DataService covid19DataService, ChartService chartService) {
        this.covid19DataService = covid19DataService;
        this.chartService = chartService;
    }

    @GetMapping("/update")
    public String updateDataset() {
        Covid19ApiResponse covid19ApiResponse = covid19DataService.getData();
        covid19DataService.saveData(covid19ApiResponse);
        return "done";
    }

    @GetMapping("/chart/{type}")
    public List<Coordinate> getChartData(@PathVariable ChartType type) {
        return chartService.getChartData(type);
    }

}
