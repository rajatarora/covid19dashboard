package co.rajat.covid19.dashboard;

import co.rajat.covid19.dashboard.models.Covid19ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class Covid19ApiConsumer {

    private String apiUrl;
    private final RestTemplate restTemplate;

    @Autowired
    public Covid19ApiConsumer(@Value("${api-url}") String apiUrl, RestTemplateBuilder builder) {
        this.apiUrl = apiUrl;
        this.restTemplate = builder.build();
    }

    public Covid19ApiResponse getCovid19Data() {
        return restTemplate.getForObject(apiUrl, Covid19ApiResponse.class);
    }

}
